FROM deplai/python

COPY ./requirements.txt /app/

RUN pip install -r /app/requirements.txt

COPY . /app/

WORKDIR /app
 
EXPOSE 3000

CMD /bin/bash -c "gunicorn djangobasicapp.wsgi:application --bind 0.0.0.0:3000 --workers 2 --log-level=info --log-file=-"
